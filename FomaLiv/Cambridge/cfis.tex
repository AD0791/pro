\chapter{Concepts Fondamentaux de l'Inf\'{e}rence Statistique}
\section{Th\'{e}orie de l'Estimation: Estimateur Ponctuel}
\paragraph{}
Dans beaucoup de r\'{e}gions on observe et enregistre des donn\'{e}es li\'{e}es \`{a} des processus naturels comme: l'intensit\'{e} de la pluie, le niveau d'eau dans une rivi\`{e}re, les vitesses du vent, la fr\'{e}quence et la magnitude d'une secousse sismique, la hauteur des vagues, les volumes de trafic $\cdots$ Parall\`{e}lement, les ing\'{e}nieurs mesurent les erreurs de topographie, ou l'efficacit\'{e} des \'{e}quipements ou d'une \'{e}quipe de construction. A partir de l\`{a}, ils essaient de d\'{e}duire des mod\`{e}les et d'estimer les param\`{e}tres associ\'{e}s.
\paragraph{}
Ainsi, un mod\`{e}le probabiliste param\'{e}tr\'{e} est la donn\'{e}e d'un ensemble $\mathcal{F}$ associ\'{e} \`{a} un nombre fini de param\`{e}tres. Par exemple, si nous supposons que les donn\'{e}es provienent d'un ph\'{e}nom\`{e}ne al\'{e}atoire mod\'{e}lisable avec une distribution normale alors on peut \'{e}crire $\mathcal{F}$ comme:
\begin{align}
\mathcal{F} = \{f(x;\mu,\sigma)= \frac{1}{\sigma \sqrt{2\pi}}\exp[-\frac{1}{2}(\frac{x-\mu}{\sigma})^{2}]\} && \mu \in \mathbb{R}, \sigma>0
\end{align}
un mod\`{e}le \`{a} deux param\`{e}tres $\mu$ et $\sigma$.
\paragraph{}
En g\'{e}n\'{e}ral, un mod\`{e}le param\'{e}trique est de la forme :
\begin{equation}
\mathcal{F} = \{f(x; \vec{\Theta}): \vec{\Theta}\in\mathbb{H}\}
\end{equation}
o\`{u} $\vec{\Theta}$ est un vecteur de param\`{e}tres inconnus qui peuvent rendre des valeurs dans l'espace $\mathbb{H}$.
\paragraph{}
Ainsi, l'objet de l'inf\'{e}rence est la caract\'{e}risation d'une population que l'on peut d\'{e}finir comme l'ensemble des individus distincts auxquels on s'int\'{e}resse. Dans la majorit\'{e} des cas, la population est trop grande pour qu'on puisse directement observer tous les individus. On est donc conduit \`{a} \'{e}tudier un sous-ensemble d'individus repr\'{e}sentatifs de cette population que l'on appelle un \'{e}chantillon. Le r\'{e}sultat $x_{i}$ de la mesure effectu\'{e}e sur le i-\`{e}me individu d'un \'{e}chantillon de n individus est une variable al\'{e}atoire $X_{i} (i=1, 2, \cdots, n)$
\paragraph{}
L'inf\'{e}rence, statistique est le processus qui consiste \`{a} utiliser des donn\'{e}es pour d\'{e}duire la distribution qui a engendr\'{e} ces donn\'{e}es. A noter qu'on fera la distinction entre la discipline appel\'{e}e Statistique (s majuscule) avec une statistique d\'{e}finie comme suit:
\subsection{Concepts Fondamentaux de l'Inf\'{e}rence Statistique}
\begin{definition}
On appelle statistique T une fonction T qui n'est exprim\'{e}e qu'en termes des variables composant l'{e}chantillon soit:
\begin{equation}
T = h(X_{1}, X_{2}, \cdots, X_{n})
\end{equation}
dont les valeurs r\'{e}alis\'{e}es seront not\'{e}es: $t=h(x_{1}, x_{2}, \cdots, x_{n})$. Puisque T est une fonction de l'\'{e}chantillon qui est un vecteur al\'{e}atoire $(x_{1}, x_{2}, \cdots, x_{n})^{t}$ dont la distribution d\'{e}pend de param\`{e}tres $\Theta_{1}, \Theta_{2}, \cdots, \Theta_{p}$ qui caract\'{e}risent la population. La statistique T est aussi une variable al\'{e}atoire dont la distribution d\'{e}pend aussi des param\`{e}tres $\Theta$ de la population. A titre d'exemples, la moyenne empirique $\bar{X} = \frac{1}{n}\sum_{i=1}^{n}X_{i}$ et la variance empirique $S^{2} = \frac{1}{n-1}\sum_{i=1}^{n}(X_{i}-\bar{X})^{2}$ sont des statistiques.
\end{definition}
\begin{definition}
Un estimateur est dit ponctuel lorsque la valeur r\'{e}alis\'{e}e d'une statistique donne une estimation de la valeur de l'un des param\`{e}tres $\Theta$ de la population ou de l'une des fonctions permettant de la caract\'{e}riser (pdf, cdf, $\cdots$). Un estimateur d'un param\`{e}tre $\Theta$ est not\'{e} $\widehat{\Theta}$ m\^{e}me quand les estimateurs des param\`{e}tres les plus courants $(\hat{\mu},\hat{\sigma}^{2})$ sont affect\'{e}s d'un symbole qui leur est propre $\overline{X}$ et $S^{2}$.
\end{definition}
\begin{definition}[Qualit\'{e} d'un bon estimateur]
	 Pour qu'une statistique puisse \^{e}tre consid\'{e}r\'{e}e comme un "bon" estimateur d'un param\`{e}tre, elle doit \^{e}tre:
	 \begin{itemize}
	 	\item[a)] sans biais \item[b)] efficace (de variance minimale) \item[c)] consistant
	 \end{itemize}
\end{definition}
\begin{definition}
	Un estimateur $\widehat{\Theta}_{n}$ d'un param\`{e}tre $\Theta$ est dit consistant si:
	\begin{equation}
	\widehat{\Theta}_{n} \overset{p}{\longrightarrow} \Theta
	\end{equation}
	C'est-\`{a}-dire $\widehat{\Theta}_{n}$ converge en probabilit\'{e} vers $\Theta$:
	\begin{equation}
	\lim\limits_{n \to \infty} prob(|\widehat{\Theta}_{n}-\Theta|<\epsilon)=1
	\end{equation}
\end{definition}
\begin{definition}
Un estimateur $\widehat{\Theta}_{1}$ est dit plus efficient qu'un estimateur $\widehat{\Theta}_{2}$ si $V(\widehat{\Theta}_{1})<V(\widehat{\Theta}_{2})$.
\end{definition}
\begin{theorem}
Si $(E(\widehat{\Theta}_{n})-\Theta) \longrightarrow 0$ et si $V(\widehat{\Theta}_{n}) \longrightarrow 0$ alors $\widehat{\Theta}_{n} \overset{p}{\longrightarrow} \Theta$ et $\widehat{\Theta}_{n}$ est consistant.
\end{theorem}
\begin{proof}
\begin{align} 
E(\widehat{\Theta}_{n}-\Theta)^{2} &= E[\widehat{\Theta}_{n}^{2}+ \Theta^{2} - 2\Theta\cdot\widehat{\Theta}_{n}]\\
&= E[\widehat{\Theta}_{n}^{2} + (E(\widehat{\Theta}_{n}))^{2}  - (E(\widehat{\Theta}_{n}))^{2} - 2\Theta\cdot\widehat{\Theta}_{n} + \Theta^{2} ]\\
E(\widehat{\Theta}_{n}-\Theta)^{2} &=  E(\widehat{\Theta}_{n}^{2}) - (E(\widehat{\Theta}_{n}))^{2}  +[(E(\widehat{\Theta}_{n}))^{2} - 2\Theta\cdot E(\widehat{\Theta}_{n}) + \Theta^{2} ]\\
&= E(\widehat{\Theta}_{n}^{2}) - (E(\widehat{\Theta}_{n}))^{2} + (E(\widehat{\Theta}_{n})-\Theta)^{2} = V(\widehat{\Theta}_{n}) + (biais)^{2}
\end{align}
en se rappelant que $V(X) = E(X^{2}) - (E(X))^{2}$. Alors puisque la convergence en moyenne quadratique implique la convergence en probabilit\'{e}:
$\widehat{\Theta}_{n}\overset{mq}{\longrightarrow}\Theta \Longrightarrow \widehat{\Theta}_{n}\overset{p}{\longrightarrow}\Theta$. Donc $\widehat{\Theta}_{n}$ est consistant. $E(\widehat{\Theta}_{n}-\Theta)^{2}$ est appel\'{e}e l'erreur quadratique moyenne (MSE Mean Square Error).
\paragraph{}
Parmi plusieurs estimateurs possibles d'un m\^{e}me param\`{e}tre on choisira celui qui a l'erreur quadratique moyenne la plus faible.
\end{proof}
\section{Estimateur par intervalle de confiance}
\paragraph{}
Jusqu'\`{a} pr\'{e}sent nous avons cherch\'{e} un estimateur $\widehat{\Theta}_{i}$ dont nous avons de bonnes raisons de croire qu'il est dans un voisinage du param\`{e}tre $\Theta_{i}$ estim\'{e}. Nous allons nous donner non pas une valeur estim\'{e}e unique mais un intervalle de valeurs qui est tel que la probabilit\'{e} de voir cet intervalle contenir la valeur $\Theta_{i}$ est proche de l'unit\'{e}. En clair, nous serons presque surs que la vraie valeur du param\`{e}tre estim\'{e} $\Theta_{i}$ se trouve dans cet intervalle.
\paragraph{}
Si l'on connait la distribution de $\widehat{\Theta}_{n}$, on peut d\'{e}terminer un $(1-\alpha)$ intervalle de confiance du param\`{e}tre $\Theta$ soit $[a,b]$ o\`{u} $a = a(X_{1}, X_{2},\cdots,X_{n})$ et $b = b(X_{1}, X_{2},\cdots,X_{n})$ fonction des donn\'{e}es tel que:
\begin{equation*}
\mathbb{P}(a\leq\Theta\leq b) \geq 1-\alpha
\end{equation*}
En clair, $[a,b]$  contient la valeur \`{a} estimer: $\Theta$ avec une probabilit\'{e} $1-\alpha$. Souvent on choisit $\alpha = 0.05$ et on est \`{a} $95\%$ certain que l'intervalle $[a,b]$ contient la vraie valeur de $\Theta$.
\begin{theorem}[Intervalle de confiance d'une distribution normale]
Supposons que  $\widehat{\Theta}_{n} \sim \mathcal{N}(\Theta,\hat{\sigma}^{2}) = \mathcal{N}(\Theta,\hat{se}^{2})$. Soit $\phi$ la fonction de r\'{e}partition (CDF) d'une loi normale centr\'{e}e r\'{e}duite $\phi(z) = \mathbb{P}(Z\leq z)$ et $z_{\frac{\alpha}{2}} = \phi^{-1}(1-(\frac{\alpha}{2}))$ et donc : $\mathbb{P}(Z>z_{\frac{\alpha}{2}}) = \frac{\alpha}{2}$ et $\mathbb{P}(-z_{\frac{\alpha}{2}}<Z<z_{\frac{\alpha}{2}}) = 1-\alpha$ avec $Z\sim\mathcal{N}(0,1)$ alors pour $C_{n} = [\widehat{\Theta}_{n}-z_{\frac{\alpha}{2}}\cdot\hat{se},\widehat{\Theta}_{n}+z_{\frac{\alpha}{2}}\cdot\hat{se}]$
\begin{equation*}
\mathbb{P}_{\Theta}(\theta \in C_{n})\longrightarrow 1-\alpha 
\end{equation*}
\end{theorem}
\begin{proof}
Soit
\begin{align}
Z_{n} &= \frac{\widehat{\Theta} - \theta}{\hat{se}} \Longrightarrow Z_{n} \sim \mathcal{N}(0,1)\\
\mathbb{P}_{\Theta}(\theta \in C_{n}) &= \mathbb{P}_{\Theta}(\widehat{\Theta}_{n}-z_{\frac{\alpha}{2}}\cdot\hat{se}<\theta<\widehat{\Theta}_{n}+z_{\frac{\alpha}{2}}\cdot\hat{se})\\
&= \mathbb{P}_{\Theta}(-z_{\frac{\alpha}{2}}< \frac{\widehat{\Theta} - \theta}{\hat{se}}<z_{\frac{\alpha}{2}})\\
&=  \mathbb{P}_{\Theta}(-z_{\frac{\alpha}{2}}<Z<z_{\frac{\alpha}{2}})
\end{align}
Ainsi pour des intervalles de confiance \`{a} $95\%$, $\alpha=0.05$ et $z_{\frac{\alpha}{2}}=1.96\sim2$ et l'approximation de l'intervalle de confiance \`{a} $95\%$ est donn\'{e}e par:
\begin{equation*}
\widehat{\Theta} \pm 2 \hat{se}
\end{equation*}
\end{proof}
\begin{example}
\begin{enumerate}
	\item  Soit un \'{e}chantillon $X_{1}, X_{2}, \cdots, X_{n}$ ind\'{e}pendant et identiquement distribu\'{e} avec $X_{i} \sim \mathcal{N}(\mu,\sigma^{2})$ alors un estimateur de la moyenne $\hat{\mu}$ est donn\'{e} par: 
	\begin{equation*}
	\hat{\mu} = \bar{X}
	\end{equation*}
	avec $\bar{X}\sim\mathcal{N}(\mu,\frac{\sigma^{2}}{n})$ alors:
	\begin{equation*}
	\mathbb{P}_{\Theta}(-z_{\frac{\alpha}{2}}< \frac{\bar{X} - \mu}{\sqrt{\cfrac{\sigma^{2}}{n}}}<z_{(1-\frac{\alpha}{2})}) = 1 - \alpha
	\end{equation*}
	la probabilit\'{e} que l'on a de se tromper en affirmant que $\mu \in [\bar{x}-z_{(1-\frac{\alpha}{2})}\cdot\sqrt{\frac{\sigma^{2}}{n}},\bar{x}+z_{(1-\frac{\alpha}{2})}\cdot\sqrt{\frac{\sigma^{2}}{n}}]$ est \'{e}gale \`{a} $\alpha$.
	\item Un estimateur de la variance est donn\'{e}e par:
	\begin{equation*}
	\hat{\sigma}^{2} = S^{2}
	\end{equation*}
	avec $\frac{(n-1)S^{2}}{\sigma^{2}}\sim\chi_{n-1}^{2}$ alors
	\begin{equation*}
	\mathbb{P}(\chi_{\frac{\alpha}{2}}^{2}\leq \frac{(n-1)S^{2}}{\sigma^{2}}\leq\chi_{1-\frac{\alpha}{2}}^{2}) = 1-\alpha
	\end{equation*}
	o\`{u} $\chi_{\frac{\alpha}{2}}^{2}$ et $\chi_{1-\frac{\alpha}{2}}^{2}$ sont respectivement les $\frac{\alpha}{2}$ et $1-\frac{\alpha}{2}$ quantiles de la loi du $\chi^{2}$ a $(n-1)$ degr\'{e}s de libert\'{e}. un intervalle de confiance au niveau $(1-\alpha)$ de $\sigma^{2}$ est donn\'{e} par
	\begin{equation*}
	\frac{(n-1)s^{2}}{\chi_{\frac{\alpha}{2}}^{2}} \leq \sigma^{2} \leq 	\frac{(n-1)s^{2}}{\chi_{1-\frac{\alpha}{2}}^{2}}
	\end{equation*}
	$s^{2}$ \'{e}tant la variance empirique de l'\'{e}chantillon $x_{1}, x_{2}, \cdots, x_{n}$
	\item Au cas o\`{u} \uline{la variance et la moyenne sont inconnues} on peut affirmer qu'un intervalle de confiance au niveau $1-\alpha$ est donn\'{e} par:
	\begin{equation*}
	\bar{x}-t_{(1-\frac{\alpha}{2})}\cdot\sqrt{\frac{\sigma^{2}}{n}} \leq \mu \leq \bar{x}+t_{(1-\frac{\alpha}{2})}\cdot\sqrt{\frac{\sigma^{2}}{n}}
	\end{equation*}
	Puique:
	\begin{equation*}
	\frac{\bar{X} - \mu}{\sqrt{\cfrac{S^{2}}{n}}} \sim t_{n-1}
	\end{equation*}
	$t_{n-1} $ \'{e}tant la distribution de Student \`{a} $n-1$ degr\'{e}s de libert\'{e}.
\end{enumerate}
\end{example}
\section{Test d'Hypoth\`{e}se}
Pour faire un test d'hypoth\`{e}ses, on d\'{e}marre avec une th\'{e}orie \`{a} d\'{e}faut - appel\'{e}e \uline{hypoth\`{e}se nulle} - et on se demande si les donn\'{e}es fournissent assez d'\'{e}vidence pour rejeter cette th\'{e}orie. L'hypoth\`{e}se contraire est appel\'{e}e \uline{l'hypoth\`{e}se alternative}. Ainsi on peut vouloir r\'{e}pondre \`{a} la question $\hat{\beta}_{j} = \beta_{j}$ o\`{u} $\beta_{j}$ est une valeur fix\'{e}e par celui ou celle qui met en oeuvre le test. Le test se note de la fa\c{c}on suivante:
\begin{equation*}
\begin{cases}
H_{0}: & \hat{\beta}_{j} = \beta_{j}\\
H_{a}: & \hat{\beta}_{j} \neq \beta_{j}
\end{cases}
\end{equation*}
\newpage