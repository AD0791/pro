\chapter{Th\'{e}orie Asymptotique}
\section{Modes de Convergence}
\begin{definition}
	Une suite de nombres r\'{e}\`{e}ls $\{\alpha_{n}\}, n=1, 2, \cdots$ est dite convergente vers un nombre r\'{e}\`{e}l $\alpha$ si pour tout $\epsilon>0$ il existe un entier $n_{0}$ tel que pour tout $n>n_{0}$ nous avons la distance de $\alpha_{n}$ \`{a} $\alpha$ soit $d(\alpha_{n}-\alpha)<\epsilon$. On ecrit alors $\alpha_{n} \longrightarrow \alpha$ o\`{u} $\lim\limits_{n \to \infty} \alpha_{n} = \alpha$.
	Cependant la notion de convergence est plus difficile \`{a} appr\'{e}hender quand il s'agit de variables al\'{e}atoires $X_{1}, X_{2}, \cdots, X_{n}$ non n\'{e}cessairement ind\'{e}pendantes. Ici on ne sait si on doit s'int\'{e}resser \`{a} la convergence des $X_{1}, X_{2}, \cdots, X_{n}$ ou \`{a} la convergence de la suite $x_{1}, x_{2}, \cdots, x_{n}$ des r\'{e}alisations des suites de variables al\'{e}atoires $X_{1}, X_{2}, \cdots, X_{n}$. Il y a ainsi diff\'{e}rents modes de convergence des suites de variables al\'{e}atoires.
\end{definition}
\begin{definition}[Convergence en Probabilit\'{e}]
	La suite de variables al\'{e}atoires $X_{1}, X_{2}, \cdots, X_{n}$ tend vers 0 en probabilit\'{e} quand $n \to \infty$ si:
	\begin{equation}
	\mathbb{P}(X_{n} \neq 0) \longrightarrow 0
	\end{equation}
	On \'{e}crit alors $X_{n}\overset{P}{\longrightarrow}0$ ou $p\lim X_{n} = 0$.\\
	De m\^{e}me on dira plus g\'{e}n\'{e}ralement qu'une suite de variables al\'{e}atoires $\{X_{n}\}$ $n = 1, 2, \cdots$ converge vers une variable a\'{e}atoire X en probabilit\'{e} si pour tout $\epsilon>0$ et tout $\sigma>0$ il existe un entier $n_{0}$ tel que pour tout $n>n_{0}$: $\mathbb{P}[d(X_{n},X)<\epsilon]>1-\sigma$. On \'{e}crit alors $X_{n}\overset{P}{\longrightarrow}X$ ou $p\lim\limits_{n \to \infty} X_{n} = X$. En clair, pour tout $\epsilon>0$: 
	\begin{eqnarray}
	\mathbb{P}(||X_{n}-X||<\epsilon) = 1\\
	\mathbb{P}(||X_{n}-X||>\epsilon) = 0
	\end{eqnarray}
\end{definition}
\begin{definition}[Convergence en Moyenne Quadratique]
	Soit $X_{1}, X_{2}, \cdots, X_{n}$ une suite de variables al\'{e}atoires telles que $E(X_{n}^{2})<\infty$. Pour tout n: $X_{n}$ converge en moyenne quadratique vers la variable al\'{e}atoire $X$ si :
	\begin{equation}
	E(X_{n}-X)^{2}\underset{n \to \infty}{\longrightarrow}0
	\end{equation}
	On note: $X_{n}\overset{M}{\longrightarrow}X$.
\end{definition}
\begin{definition}[Convergence en Distribution]
	Une suite $X_{1}, X_{2}, \cdots, X_{n}$ de fonction de r\'{e}partition (distribution) $F_{n}$ est dite convergente en distribution vers X si la distribution $F_{n}$ de $X_{n}$ converge vers la distribution F de X en tout point de continuit\'{e} de F et on \'{e}crit: $X_{n}\overset{d}{\longrightarrow}X$.\\
	Ces 3 modes de convergence s'articulent comme suit:\\
	$\fbox{Convergence en Moyenne Quadratique}\Longrightarrow\fbox{Convergence en Probabilit\'{e}}\Longrightarrow\fbox{Convergence en Distribution}$.
\end{definition}
Pour le montrer, on va utiliser les in\'{e}galit\'{e}s  de Chebychev et de Markov qui constituent les th\'{e}or\`{e}mes ci-apr\`{e}s.
\begin{theorem}[In\'{e}galit\'{e} de Chebychev]
	Pour une variable al\'{e}atoire X quelconque de moyenne $\mu$:
	\begin{align}
		\mathbb{P}(|X-\mu| \geq a) \leq \frac{V[X]}{a^{2}} && \forall a>0
	\end{align}
\end{theorem}
\begin{proof}
	On sait que:
	\begin{align}
		V(X) = \int_{-\infty}^{\infty}{(x-\mu)^{2}f(x)d(x)} = \int_{-\infty}^{\mu- a}{(x-\mu)^{2}f(x)d(x)}& + \int_{\mu-a}^{\mu+ a}{(x-\mu)^{2}f(x)d(x)}+\int_{\mu+a}^{\infty}{(x-\mu)^{2}f(x)d(x)}\\
		\int_{-\infty}^{\infty}{(x-\mu)^{2}f(x)d(x)} \geq \int_{-\infty}^{\mu-a}{(x-\mu)^{2}f(x)d(x)} &+ \int_{\mu+a}^{\infty}{(x-\mu)^{2}f(x)d(x)}\\
		x\in]\infty,\mu-a[ \Longrightarrow x<\mu-a \Longrightarrow (x-\mu)^{2}>a^{2} &\Longrightarrow \int_{-\infty}^{\mu-a}{(x-\mu)^{2}f(x)d(x)} \geq a^{2} \int_{-\infty}^{\mu-a}{f(x)d(x)}\\
		x\in]\mu+a,\infty[ \Longrightarrow x>\mu+a \Longrightarrow (x-\mu)^{2}>a^{2} &\Longrightarrow \int_{\mu+a}^{\infty}{(x-\mu)^{2}f(x)d(x)} \geq a^{2} \int_{\mu+a}^{\infty}{f(x)d(x)}
	\end{align}
	Donc
	\begin{align}
		V(x) = \int_{-\infty}^{\infty}{(x-\mu)^{2}f(x)d(x)} &\geq a^{2}[\int_{-\infty}^{\mu-a}{f(x)d(x)} + \int_{\mu+a}^{\infty}{f(x)d(x)}] = a^{2}[\mathbb{P}(X \leq \mu-a) +\mathbb{P}(X \geq \mu+a)]\\
		&= a^{2}[\mathbb{P}(X-\mu \leq -a) +\mathbb{P}(X-\mu \geq a)]\\
		V(X) &\geq a^{2}[\mathbb{P}(|x-\mu|\geq a)]
	\end{align}
\end{proof}
\begin{example}
	Si l'on sait que la pluviom\'{e}trie annuelle dans une r\'{e}gion vaut en moyenne 800 mm avec un \'{e}cart-type de 100 mm. On peut utiliser l'in\'{e}galit\'{e} de Ch\'{e}bychev pour conna\^{i}tre la probabilit\'{e} qu'il tombe durant une ann\'{e}e entre 600 mm et 1000 mm  de pluie.
	Soit $\mu=800$ et $\sigma=100$ alors pour $\alpha=200$, l'in\'{e}galit\'{e} donne : 
	\begin{equation*}
		\mathbb{P}(|X-800|\geq 200)\leq \frac{100^{2}}{200^{2}} =\frac{1}{4}
	\end{equation*}
	Puisque
	\begin{align*}\mathbb{P}(|X-800|>200) &= \mathbb{P}(X-800>200) + \mathbb{P}(X-800<-200)\\ 
		&= \mathbb{P}(X >1000) + \mathbb{P}(X<600)\end{align*}
	Mais
	\begin{equation}
	\mathbb{P}(X >1000) + \mathbb{P}(X<600) = 1 - \mathbb{P}(600\leq X \leq 1000)
	\end{equation}
	Donc
	\begin{eqnarray}
	\mathbb{P}(600\leq X \leq 1000) &=& 1 - \{ \mathbb{P}(X >1000) + \mathbb{P}(X<600)\}\\
	&=& 1-\mathbb{P}(|X-800|>200) > 1 - \frac{1}{4} =  \frac{3}{4} 
	\end{eqnarray}
\end{example}
\subsection{Th\'{e}or\`{e}me Fondamental de la Th\'{e}orie Symptotique}
\begin{align}
	X_{n} \overset{M}{\longrightarrow} X \Longrightarrow X_{n} \overset{P}{\longrightarrow} X\\
	X_{n} \overset{P}{\longrightarrow} X \Longrightarrow X_{n} \overset{d}{\longrightarrow} X
\end{align}
Pour prouver (4.1.16) on va utiliser l'in\'{e}galit\'{e} dite de \uline{Markov}. 
\begin{theorem}[In\'{e}galit\'{e} de Markov]
	Soit $\mathsf{X}$ une variable al\'{e}atoire positive; supposons que $E(X)$ existe: alors pour tout $\alpha>0$
	\begin{equation}
	\mathbb{P}(X>\alpha) \leq \frac{E(X)}{\alpha}
	\end{equation}
\end{theorem}
\begin{proof}
	En effet, puisque $X>0$ 
	\begin{align}
		E(X)=& \int_{0}^{\infty}{xf(x)dx} = \int_{0}^{\alpha}{xf(x)dx} + \int_{\alpha}^{\infty}{xf(x)dx}\\
		E(X) \geq& \int_{\alpha}^{\infty}{xf(x)dx} \geq \alpha \int_{\alpha}^{\infty}{f(x)dx} = \alpha \mathbb{P}(X>\alpha) \Longrightarrow \mathbb{P}(X>\alpha)\leq\frac{E(X)}{\alpha}
	\end{align}
	Alors on peut, d'apr\`{e}s l'in\'{e}galit\'{e} de Chebychev, d\'{e}montrer (4.1.11) en remarquant :
	\begin{equation}
	\mathbb{P}(|X_{n}-X|>a) = \mathbb{P}((X_{n}-X)^{2}>a^{2}) \leq \frac{E(X_{n}-X)^{2}}{a^{2}}
	\end{equation}
	Mais puisque $X_{n}\overset{M}{\longrightarrow}X$ alors $E(X_{n}-X)^{2} \longrightarrow 0$ quand $n \to \infty$ et donc il en est de m\^{e}me de $\mathbb{P}(|X_{n}-X|>\alpha)$ pour $\forall \alpha>0$. D'o\`{u}  \fbox{$X_{n} \overset{M}{\longrightarrow} X \Longrightarrow X_{n} \overset{P}{\longrightarrow} X$}.
	\paragraph{}
	La d\'{e}monstration de (4.1.17) est plus subtile. Soit alors:
	\begin{eqnarray}
	A &=& \{X_{n}<x\}\\
	E &=& \{X\leq x+\epsilon\}\\
	E^{c} &=& \{X>x+\epsilon\}
	\end{eqnarray}
	Nous fixons $\epsilon>0$ et choisissons x un point de continuit\'{e} de F alors: 
	\begin{equation}
	\mathbb{P}\{A\} = \mathbb{P}(A \cap E) + \mathbb{P}(A \cap E^{c}) = \mathbb{P}(A|E)\mathbb{P}(E) + \mathbb{P}(A \cap E^{c}) 
	\end{equation}
	Mais:
	\begin{eqnarray}
	F_{n}(x) &=& \mathbb{P}\{X_{n}<x\} = \mathbb{P}(X_{n}\leq x, X \leq x+\epsilon) + \mathbb{P}(X_{n}\leq x, X>x+\epsilon)\\
	&=& \mathbb{P}\{X_{n}\leq x|X\leq x+\epsilon\}*\mathbb{P}(X\leq x+\epsilon) + \mathbb{P}(X_{n}\leq x, X>x+\epsilon)
	\end{eqnarray}
	Donc:
	\begin{equation}
	F_{n}(x) \leq \mathbb{P}(X\leq x+\epsilon) + \mathbb{P}(X_{n}\leq x, X>x+\epsilon)
	\end{equation}
	Consid\'{e}rons l'\'{e}v\`{e}nement: $\{X_{n} \leq x, X>x+\epsilon\}$. Alors soit:
	\begin{equation}
	X>x+\epsilon \Longrightarrow X-X_{n}>x+\epsilon-X_{n} \Longrightarrow X-X_{n}>\epsilon
	\end{equation}
	car $x\geq X_{n}$
	\begin{equation}
	-X<-x-\epsilon \Longrightarrow X_{n}-X<X_{n}-\epsilon-x=-\epsilon-(x-X_{n}) \Longrightarrow X_{n}-X<-\epsilon
	\end{equation}
	L'\'{e}v\'{e}nement $\{X_{n} \leq x,  X>X+\epsilon\}$ est donc \'{e}quivalent \`{a}: $|X_{n}-X|>\epsilon$ et donc $\mathbb{P}\{X_{n} \leq x, X>X+\epsilon\} = \mathbb{P}(|X_{n}-X|>\epsilon)$. Puisque $F(x+\epsilon) = \mathbb{P}(X\leq x + \epsilon)$ et utilisant (4.1.28)
	\begin{equation}
	F_{n}(x) \leq F(x+\epsilon) + \mathbb{P}(|X_{n}-X|>\epsilon)
	\end{equation} 
	Par raisonnement analogue avec les 3 \'{e}v\`{e}nements A, E et $E^{c}$ et utilisant le th\'{e}or\`{e}me des probabilit\'{e}s totales:
	\begin{eqnarray}
	F(x-\epsilon) &=& \mathbb{P}(X\leq x-\epsilon, X_{n} \leq x) + \mathbb{P}(X\leq x-\epsilon, X_{n}> x)\\
	&=& \mathbb{P}(X\leq x-\epsilon|X_{n} \leq x)\mathbb{P}(X_{n}\leq x) + \mathbb{P}(X\leq x-\epsilon, X_{n}> x)\\
	&\leq& \mathbb{P}(X_{n}\leq x) + \mathbb{P}(X\leq x-\epsilon, X_{n}> x)\\
	&\leq& \mathbb{P}(X_{n}\leq x) + \mathbb{P}(|X_{n}-X|> \epsilon)
	\end{eqnarray}
	Donc
	\begin{eqnarray}
	F(x-\epsilon) &\leq& F_{n}(x) + \mathbb{P}(|X_{n}-X|> \epsilon)\\
	F(x-\epsilon)-\mathbb{P}(|X_{n}-X|> \epsilon) &\leq& F_{n}(x) \leq F(x+\epsilon) + \mathbb{P}(|X_{n}-X|> \epsilon)
	\end{eqnarray}
	Quand $n\to \infty$ :
	\begin{equation}
	F(x-\epsilon) \leq \lim\limits_{n\to \infty}\inf F_{n}(x)\leq \lim\limits_{n\to \infty}\sup F_{n}(x)\leq F(x+\epsilon)
	\end{equation}
	Comme c'est vrai pour tout $\epsilon$ c'est vrai aussi quand $\epsilon\to 0$. D'o\`{u}: 
	\begin{equation}
	\lim\limits_{n \to \infty}F_{n}(x) = F(x)
	\end{equation}
	et \fbox{$X_{n} \overset{P}{\longrightarrow} X \Longrightarrow X_{n} \overset{d}{\longrightarrow} X$}
\end{proof}
\section{La Loi Faible des Grands Nombres (WLLN) et Th\'{e}or\`{e}me Central Limite}
\subsection{ Loi Faible des Grands Nombres}
\begin{theorem}[ WLLN]
	Soient $X_{1}, X_{2}, X_{3}, \cdots, X_{n}$ un \'{e}chantillon de variables al\'{e}atoires i.i.d avec $\mu = E(X_{i})$ et $V(X_{i}) = \sigma^{2}$ alors:
	\begin{equation}
	\overline{X_{n}} \overset{P}{\longrightarrow} \mu
	\end{equation}
\end{theorem}
\begin{proof}
	\begin{eqnarray}
	\overline{X_{n}} &=& \frac{1}{n}\sum_{i=1}^{n}X_{i}\Longrightarrow E(\overline{X_{n}}) = \frac{1}{n}\sum_{i=1}^{n}E(X_{i}) = \mu\\
	V(\overline{X_{n}}) &=& v(\frac{1}{n}\sum_{i=1}^{n}X_{i})= \frac{1}{n^{2}}V(\sum_{i=1}^{n}X_{i})= \frac{n\sigma^{2}}{n^{2}}= \frac{\sigma^{2}}{n}
	\end{eqnarray}
	Utilisant Chebychev:
	\begin{equation}
	\mathbb{P}(|\overline{X_{n}}-\mu| \geq \epsilon) \leq \frac{V(\overline{X_{n}})}{\epsilon^{2}}
	\end{equation}
	Qui tend vers 0 quand $n\to \infty$ car $V(\bar{X}_{n}) = \frac{\sigma^{2}}{n}$
	\begin{equation}
	\overline{X_{n}} \overset{P}{\longrightarrow}\mu
	\end{equation}
\end{proof}
\subsection{Th\'{e}or\`{e}me Central Limite}
\begin{theorem}
	Soient $X_{1}, X_{2}, X_{3}, \cdots, X_{n}$ i.i.d de moyenne $\mu$ et de variance $\sigma^{2}$ alors $\bar{X}_{n} = n^{-1}\sum_{i=1}^{n}X_{i}$ est tel que:
	\begin{equation}
	Z_{n} = \frac{\overline{X_{n}}-\mu}{\sqrt{V(\overline{X_{n}})}} = \frac{\sqrt{n}(\overline{X_{n}}-\mu)}{\sigma} \overset{P}{\longrightarrow} \mathcal{N}(0,1)
	\end{equation}
	En clair $(avec \sigma<\infty)$
	\begin{equation} 
	\lim\limits_{n \to \infty} \mathbb{P}(Z_{n}\leq z) = \int_{-\infty}^{z}{\frac{1}{\sqrt{2\pi}}\exp(-\frac{x^{2}}{2})dx}
	\end{equation}
\end{theorem}
\begin{proof}
	On fait l'hypoth\`{e}se suppl\'{e}mentaire que la fonction g\'{e}n\'{e}ratrice des moments $M_{x}(t)$ des $X_{i}$ existe pour tout $|t|<h$ et  que la d\'{e}riv\'{e}e $3^{ieme}$ de $M_{x}$ est Born\'{e}e par $\mathsf{B}$ pour tout $|t|<h$. Comme les $X_{i}$ sont ind\'{e}pendants et indentiquement distribu\'{e}s. Il en est de m\^{e}me de :
	\begin{equation}
	Y_{i} = \frac{(X_{i}-\mu)}{\sigma}
	\end{equation}
	Ces $Y_{i}$ sont aussi donc i.i.d. Nous allons montrer que la f.g.m: $M_{n}(t)$ de la variable al\'{e}atoire:
	\begin{equation}
	\sqrt{n}\cdot\bar{Y}_{n} = \frac{1}{\sqrt{n}}\sum_{i=1}^{n}Y_{i} = \frac{1}{\sqrt{n}}\sum_{i=1}^{n}(\frac{X_{i}-\mu}{\sigma}) = \frac{\sqrt{n}(\overline{X_{n}}-\mu)}{\sigma}  
	\end{equation}
	converge vers $\exp(\frac{t^{2}}{2})$ qui est la f.g.m de $\mathcal{N}(0,1)$.
	\paragraph{}
	La fonction g\'{e}n\'{e}ratrice des moments de $\mathsf{Y_{i}}$ est:
	\begin{equation}
	M_{Y}(t) = E[\exp(tY_{i})] = E[\exp(\frac{t(X_{i}-\mu)}{\sigma})] = e^{-\frac{t\mu}{\sigma}}E[\exp(\frac{tX_{i}}{\sigma})] = e^{-\frac{t\mu}{\sigma}}M_{X}(\frac{t}{\sigma}) 
	\end{equation}
	qui existe et a sa d\'{e}riv\'{e}e troisi\`{e}me born\'{e}e. Alors la fonction g\'{e}n\'{e}ratrice des moments de la variable al\'{e}atoire $\sqrt{n}\cdot\bar{Y}_{n}$ vaut:
	\begin{eqnarray}
	M_{n}(t) &=& E[\exp(\frac{t}{\sqrt{n}}\sum_{i=1}^{n}Y_{i})]\\
	&=& E[\prod_{i=1}^{n}\exp(\frac{t}{\sqrt{n}}Y_{i})] = [M_{Y}(\frac{t}{\sqrt{n}})]^{n}
	\end{eqnarray}
	D'apr\`{e}s le d\'{e}veloppement en s\'{e}rie de Taylor:
	\begin{equation}
	M_{Y}(\frac{t}{\sqrt{n}}) \simeq M{Y}(0) + \frac{\partial(M_{Y}(0))}{\partial t}(\frac{t}{\sqrt{n}}) + \frac{1}{2!}\frac{\partial^{2}(M_{Y}(0))}{\partial t^{2}}(\frac{t}{\sqrt{n}})^{2} + \frac{1}{2!}\frac{\partial^{3}(M_{Y}(0))}{\partial t^{3}}(\frac{t}{\sqrt{n}})^{3} + \cdots
	\end{equation}
	Car:
	\begin{equation*}
		f(x_{0}+h) \simeq \sum_{k=0}^{n} \frac{h^{k}}{k!}f^{(k)}(x_{0}) + R_{n}(h)
	\end{equation*}
	Or :
	\begin{itemize}
		\item[1)] La d\'{e}riv\'{e}e d'ordre 3 est born\'{e}e par $B_{n}$.
		\item[2)] $M_{Y}(0)=1$ car $Y_{i}$ a pour moyenne 0 et variance 1.
		\item[3)] \begin{equation*}
			\frac{\partial(M_{Y}(0))}{\partial t} = 0
		\end{equation*}
		\item[4)] \begin{equation*}
			\frac{\partial^{2}(M_{Y}(0))}{\partial t^{2}} = 1
		\end{equation*}
	\end{itemize}
	Alors:
	\begin{eqnarray}
	M_{y}(\frac{t}{\sqrt{n}}) &\simeq& 1 + \frac{1}{2}(\frac{t}{\sqrt{n}})^{2} + \frac{B_{n}}{6}(\frac{t}{\sqrt{n}})^{3} \\
	&\simeq& 1 + \frac{1}{n}[\frac{t^{2}}{2} + \frac{B_{n}}{6}\frac{t^{3}}{\sqrt{n}}] \\
	M_{n}(t) \simeq \{1 + \frac{1}{n}[\frac{t^{2}}{2} + \frac{B_{n}}{6}\frac{t^{3}}{\sqrt{n}}]\}^{n}
	\end{eqnarray}
	Mais si $\lim\limits_{n \to \infty} a_{n} =a$ alors:
	\begin{equation*}
		\lim\limits_{n \to \infty}(1+\frac{a_{n}}{n})^{n} = e^{a}
	\end{equation*}
	Donc quand $n \to \infty$, $M_{n}(t) \longrightarrow \exp(\frac{t^{2}}{2})$ qui est la fonction g\'{e}n\'{e}ratrice des moments de $\mathcal{N}(0,1)$.
\end{proof}
\begin{example}[Applications du Th\'{e}or\`{e}me Central Limite]
	On rappelle que si X est une distribution binomiale $X \sim \mathcal{B}(n,p)$ avec $E(X) = np$ et $V(X) = np(1-p)$.
	\begin{itemize}
		\item La loi des grands nombres (Khinchine) postule que $\{X_{i}\}$ sont i.i.d avec $E(X_{i}) = \mu$ alors $\bar{X}_{n} \overset{P\phantom{ppp}}{\longrightarrow \mu}$.
		\item Le th\'{e}or\`{e}me central limite dit: si les $\{X_{i}\}$  sont i.i.d avec $E(X_{i}) = \mu$  et $V(X_{i}) = \sigma^{2}$ alors
		\begin{equation*}
			Z_{n} = \frac{1}{\sqrt{V(\bar{X}_{n})}}(\bar{X}_{n}-E(\bar{X}_{n})) \overset{d\phantom{000000000}}{\longrightarrow \mathbb{N}(0,1)}
		\end{equation*}
		Quand n est grand
	\end{itemize}
	\paragraph{}
	Si $X \sim \mathcal{B}(n,p)$ $X = \sum_{i=1}^{n}Y_{i}$ o\`{u} $Y_{i}$ est une variable de Bernoulli donc qui prend la valeur 1 avec une probabilit\'{e} p et 0 avec une probabilit\'{e} $(1-p)$. Puisque les $Y_{i}$ sont i.i.d avec $E(Y_{i})  = p$ et $V(Y_{i}) = p(1-p)$ alors:
	\begin{align*}
		\frac{1}{\sqrt{V(\bar{Y}_{n})}}\cdot(\bar{Y}_{n}-E(\bar{Y}_{n})) &= 	\frac{1}{\sqrt{n^{-1}p(1-p)}}\cdot(\frac{X}{n}-\frac{np}{n}) \overset{d\phantom{0000000000}}{\longrightarrow \mathcal{N}(0,1)}\\
		\frac{\frac{X}{n}-p}{\sqrt{\frac{p(1-p)}{n}}}&\overset{d\phantom{0000000000}}{\longrightarrow \mathcal{N}(0,1)}
	\end{align*}
	Cependant on pr\'{e}f\`{e}re \'{e}crire $\frac{\frac{X}{n}-p}{\sqrt{\frac{p(1-p)}{n}}}\overset{A\phantom{0000000000}}{\sim \mathcal{N}(0,1)}$ et dire que $\frac{X}{n}$ converge asymptotiquement vers une loi normale de moyenne p et de variance $\frac{p(1-p)}{n}$.
	\paragraph{}
	D'une mani\`{e}re g\'{e}n\'{e}rale: si X suit une loi binomiale de param\`{e}tres n,p. Si n est mod\'{e}r\'{e}ment grand($np\geq5$ et $np(1-p)\geq5$) alors X est asymptotiquement normalement distribu\'{e}. Si nous voulons calculer $\mathbb{P}(a\leq X \leq b)$ une bonne approximation est donn\'{e}e par: $\mathbb{P}(a\leq X \leq b)\simeq\mathbb{P}(a\leq Y \leq b)$ o\`{u} Y est normalement distribu\'{e}e avec $\mu = np$ et $V(Y) = np(1-p)$.
	\paragraph{}
	A titre d'exemple supposons que Alpha Electronics a un taux de d\'{e}faut dans sa cha\^{i}ne d'assemblage de semi-conducteurs de 1 pour mille. Quelle est la vraisemblance que l'on retrouve dix "boards" au moins d\'{e}fecteux dans un batch de 6 000.
	\paragraph{}
	Soit X le nombre de "board" d\'{e}fectueux dans un bac de 6 000. X suit une distribution binomiale de param\`{e}tres $p = \frac{1}{1 000}$ et $n = 6 000$.
	\begin{align*}
		\mu &=p\cdot n = 6\\
		\sigma &= \sqrt{np(1-p)} = 2.448 
	\end{align*}
	Comme $np = 6 > 5$ et $n(1-p) = 5.994 > 5$. X peut \^{e}tre approch\'{e} asymptotiquement par une variable Y normale de moyenne $\mu = 6$ et d'\'{e}cart-type $\sigma = 2.448$. Alors
	\begin{align*}
		\mathbb{P}(X\geq 10) \simeq \mathbb{P}(Y \geq 10)&= \mathbb{P}(\frac{Y-6}{2.448}\geq \frac{10-6}{2.448})\\
		&= \mathbb{P}(Z\geq 1.63)\\
		\mathbb{P}(X\geq 10) &\simeq 0.0516
	\end{align*}
	\paragraph{}
	Le th\'{e}or\`{e}me Central Limite (CLT) nous dit que $Z_{n} = \frac{\sqrt{n}(\overline{X_{n}}-\mu)}{\sigma} \overset{P}{\longrightarrow} \mathcal{N}(0,1)$. Mais nous connaissons rarement $\sigma$. Un estimateur ponctuel  de $\sigma^{2}$ est donn\'{e} par:
	\begin{equation}
	S_{n}^{2} = \frac{1}{n-1}\sum_{i=1}^{n}(X_{i}-\bar{X}_{n})^{2}
	\end{equation}
	qui est aussi une variable al\'{e}atoire dont l'\'{e}sp\'{e}rance est :
	\begin{eqnarray}
	E(S_{n}^{2}) &=& \frac{1}{n-1}E[\sum_{i=1}^{n}(X_{i}-\bar{X}_{n})^{2}] = \frac{1}{n-1}E[\sum_{i=1}^{n}\{(X_{i}-\mu)-(\bar{X}_{n}-\mu)\}^{2}]\\
	&=& \frac{1}{n-1}\{\sum_{i=1}^{n}E(X_{i}-\mu)^{2}-nE(\bar{X}_{n}-\mu)^{2}\} 
	\end{eqnarray}
	Mais:
	\begin{eqnarray*}
		E(X_{i}-\mu)^{2} &=& \sigma^{2}\\
		E(\overline{X_{n}}-\mu)^{2} &=& \frac{\sigma^{2}}{n}
	\end{eqnarray*}
	Donc:
	\begin{equation}
	E(S_{n}^{2}) = \frac{1}{n-1}(n\sigma^{2}-\sigma^{2}) = \sigma^{2}
	\end{equation}
	\uline{La variance de l'\'{e}chantillon est donc un estimateur non biais\'{e} de $\sigma^{2}$}. Or:
	\begin{eqnarray}
	(n-1)S_{n}^{2} &=& \sum_{i=1}^{n}(X_{i}-\bar{X}_{n})^{2} = \sum_{i=1}^{n}(X_{i}+\mu-\mu-\bar{X}_{n})^{2} =\sum_{i=1}^{n}\{(X_{i}-\mu)-(\bar{X}_{n}-\mu)\}^{2}\\
	&=& \sum_{i=1}^{n}\{(X_{i}-\mu)^{2}\} - n(\bar{X}_{n}-\mu)^{2} = [\sum_{i=1}^{n}(X_{i}-\mu)^{2}]-n(\bar{X}_{n}-\mu)^{2}\\
	\frac{n-1}{\sigma^{2}}S_{n}^{2} &=& [\sum_{i=1}^{n}(\frac{X_{i}-\mu}{\sigma})^{2}] - (\frac{\bar{X}_{n}-\mu}{\frac{\sigma}{\sqrt{n}}})^{2}
	\end{eqnarray}
	Le premier membre de l'\'{e}quation (3.62) est distribu\'{e} comme un $\chi^{2}$ \`{a} n degr\'{e}s de libert\'{e} et le second comme un $\chi^{2}$ \`{a} 1 degr\'{e} de libert\'{e}. Donc $\frac{n-1}{\sigma^{2}}S_{n}^{2}$ est distribu\'{e} comme un $\chi^{2}$ \`{a} $n-1$ degr\'{e}s de libert\'{e}. 
	\paragraph{}
	Mieux,
	\begin{equation}
	\overset{P}{S_{n}^{2} \longrightarrow \sigma^{2}}
	\end{equation}
	\textbf{On dit que c'est un estimateur consistant  de $\sigma^{2}$}.
	En effet en vertu de l'in\'{e}galit\'{e} de Chebychev:
	\begin{equation}
	\mathbb{P}(|S_{n}^{2}-\sigma^{2}|\geq \epsilon) \leq \frac{(S_{n}^{2}-\sigma^{2})^{2}}{\epsilon^{2}} = \frac{V(S_{n}^{2})}{\epsilon^{2}}
	\end{equation}
	Une condition suffisante pour que $\overset{P}{S_{n}^{2} \longrightarrow \sigma^{2}}$ est donc que $V(S_{n}^{2}) \longrightarrow 0$ quand $n \to \infty$.
\end{example}
\section{La M\'{e}thode Delta}
\begin{theorem}
	Supposons que:
	\begin{equation}
	\frac{\sqrt{n}(Y_{n}-\mu)}{\sigma} \overset{d}{\longrightarrow} \mathcal{N}(0,1)
	\end{equation}
	Et que g est une fonction diff\'{e}rentiable "suffisamment r\'{e}guli\`{e}re" telle que $g^{\prime}(\mu) \neq 0$. Alors:
	\begin{equation}
	\frac{\sqrt{n}(g(Y_{n})-g(\mu))}{|g^{\prime}(\mu)|\sigma} \overset{d}{\longrightarrow} \mathcal{N}(0,1)
	\end{equation}
\end{theorem}
En clair: 
\begin{equation}
Y_{n} \sim \mathcal{N}(\mu, \frac{\sigma^{2}}{n}) \Longrightarrow g(Y_{n}) \sim \mathcal{N}(g(\mu), g^{\prime}(\mu)^{2}\frac{\sigma^{2}}{n})
\end{equation} 
Pour montrer ce r\'{e}sultat on va utiliser 2 lemmes.
\begin{lemma}[Slutsky]
	Si $\overset{d}{X_{n} \longrightarrow X}$ et $\overset{p}{Y_{n} \longrightarrow a}$, avec $a \in \mathbb{R}$ alors quand $n \to \infty$
	\begin{align}
		\overset{d}{Y_{n}X_{n} \longrightarrow aX}\\
		\overset{d}{Y_{n}+X_{n} \longrightarrow a+X}
	\end{align}
\end{lemma}
\begin{lemma}
	Si $\sqrt{n}(Y_{n}-\mu) \overset{d}{\longrightarrow} \mathcal{N}(0,\sigma^{2})$ quand $n \to \infty$ alors:
	\begin{equation}
	\overset{p}{Y_{n} \longrightarrow \mu}
	\end{equation}
\end{lemma}
\begin{proof}(Du Lemme 3.3.3)~\\
	Comme
	\begin{equation}
	\overset{d}{\sqrt{n}(Y_{n}-\mu) \longrightarrow \mathcal{N}(0,\sigma^{2})} \Longrightarrow \overset{d}{\sqrt{n}(\frac{Y_{n}-\mu}{\sigma}) \longrightarrow \mathcal{N}(0,\frac{\sigma^{2}}{n})}
	\end{equation}
	Etant donn\'{e} l'in\'{e}galit\'{e} de Chebychev
	\begin{align}
		\mathbb{P}(|Y_{n}-\mu| \geq \epsilon) \leq \frac{V[Y_{n}]}{\epsilon^{2}} \longrightarrow \frac{\sigma^{2}}{n\epsilon^{2}} \longrightarrow 0  &&\forall n \to \infty
	\end{align}
	Donc
	\begin{equation}
	\overset{p}{Y_{n} \longrightarrow \mu}
	\end{equation}
\end{proof}
\begin{proof}
	Pour montrer le th\'{e}or\`{e}me dit \textbf{DELTA METHOD} on va \'{e}crire le th\'{e}or\`{e}me des valeurs interm\'{e}diaires (T.V.I) pour $g(Y_{n})$ autour de $Y_{n}=\mu$. C'est-\`{a}-dire qu'il existe $\tilde{\mu}_{n}$ tel que $Y_{n}<\tilde{\mu}_{n}<\mu$ et:
	\begin{equation}
	g(Y_{n}) = g(\mu)+ \frac{\partial g(\tilde{\mu}_{n})}{\partial \mu}\cdot(Y_{n}-\mu)
	\end{equation}
	\paragraph{}
	En vertu du lemme 2 et puisque $Y_{n}<\tilde{\mu}<\mu$, on doit avoir quand $n \to \infty$:
	\begin{equation}
	\overset{p}{ \tilde{\mu}_{n} \longrightarrow \mu}
	\end{equation}
	Ensuite selon le th\'{e}or\`{e}me de l'application continue (continuous mapping theorem)
	\begin{equation}
	\frac{\partial}{\partial \mu} (g(\tilde{\mu}_{n})) \overset{p}{\longrightarrow} \frac{\partial}{\partial \mu} (g(\mu))
	\end{equation}
	Car g est diff\'{e}rentiable de d\'{e}riv\'{e}e continue. Alors comme:
	\begin{equation}
	\sqrt{n}[g(Y_{n})-g(\mu)] = \sqrt{n}\frac{\partial}{\partial \mu} (g(\tilde{\mu}_{n}))(Y_{n}-\mu)
	\end{equation}
	Mais selon l'hypoth\`{e}se du th\'{e}or\`{e}me de la m\'{e}thode de Delta
	\begin{equation}
	\sqrt{n}(Y_{n}-\mu) \overset{d}{\longrightarrow} \mathcal{N}(0,\sigma^{2}) \Longleftrightarrow \frac{Y_{n}-\mu}{(\cfrac{\sigma}{\sqrt{n}})}\overset{d}{\longrightarrow} \mathcal{N}(0,1)
	\end{equation}
	et
	\begin{equation}
	\frac{\partial}{\partial \mu} (g(\tilde{\mu}_{n})) \overset{p}{\longrightarrow} \frac{\partial}{\partial \mu} (g(\mu))
	\end{equation}
	Donc en vertu du th\'{e}or\`{e}me de Slutsky:
	\begin{equation}
	\sqrt{n}(Y_{n}-\mu)*\frac{\partial}{\partial \mu} (g(\tilde{\mu}_{n})) \overset{d}{\longrightarrow} \frac{\partial}{\partial \mu} (g(\mu))*\mathcal{N}(0,\sigma^{2})=\mathcal{N}(0,\sigma^{2}[g^{\prime}(\mu)]^{2}) 
	\end{equation}
	Donc en vertu de (3.77)
	\begin{equation}
	\frac{\sqrt{n}(g(Y_{n})-g(\mu))}{|g^{\prime}(\mu)|\sigma}\overset{d}{\longrightarrow} \mathcal{N}(0,1)
	\end{equation}
	D'o\`{u}
	\begin{equation}
	Y_{n} \longrightarrow \mathcal{N}(\mu,\frac{\sigma^{2}}{n}) \Longrightarrow g(Y_{n}) \overset{p}{\longrightarrow} \mathcal{N}(g(\mu),g^{\prime}(\mu)^{2}\frac{\sigma^{2}}{n}
	\end{equation}
\end{proof}
\begin{example}
	Ainsi si $X_{1}, X_{2}, \cdots, X_{n}$ i.i.d de moyenne $\mu$ et de variance finie $\sigma^{2}$ en vertu du th\'{e}or\`{e}me central limite (CLT)
	\begin{equation}
	\sqrt{n}\frac{(\bar{X}_{n}-\mu)}{\sigma} \overset{d}{\longrightarrow} \mathcal{N}(0,1)
	\end{equation}
	Soit alors $W_{n} = \exp(\bar{X}_{n})$,  c'est-\`{a}-dire $g(s)=\exp(s)$, $W_{n}=g(\bar{X}_{n})$ et $g^{\prime}(s) = \exp(s)$. En vertu du Delta method:
	\begin{equation}
	W_{n} \sim \mathcal{N}(\exp(\mu),\exp(2\mu)\frac{\sigma^{2}}{n})
	\end{equation}
\end{example}
\section{Compl\'{e}ments sur les Vecteurs Al\'{e}atoires}
\paragraph{}
Soit $\vec{x}$ un vecteur dont les n composantes sont des variables al\'{e}atoires telles que:
\begin{equation}
\underset{(n,1)}{\vec{x}} = 
\begin{pmatrix}
x_{1}\\
x_{2}\\
\vdots\\
x_{n}
\end{pmatrix}
\end{equation}
Alors:
\begin{itemize}
	\item[a)] $E(\vec{x})$ est d\'{e}finie par:
	\begin{equation}
	E(\vec{x}) = 
	\begin{pmatrix}
	E(x_{1})\\
	E(x_{2})\\
	\vdots\\
	E(x_{n})
	\end{pmatrix}
	\end{equation}
	\item[b)] La matrice de variance-covariance de $\vec{x}$ est d\'{e}finie par:
	\begin{equation}
	V(\vec{x}) = E[(\vec{x}-E(\vec{x}))(\vec{x}-E(\vec{x}))^{t}]
	\end{equation}
	\uline{NB}
	\begin{itemize}
		\item $V(\vec{x})$ est une matrice sym\'{e}trique.
		\item Les termes de sa diagonale sont les variances $V(x_{i})$ des composantes $x_{i}$ de $\vec{x}$.
		\item Les autres termes sont les covariances $cov(x_{i},x_{j})$ des composantes de $\vec{x}$:
		\begin{equation}
		V(\vec{x}) = E[(x_{i}-E(x_{i}))(x_{j}-E(x_{j}))]
		\end{equation}
		\item $V(x)$ est d\'{e}finie positive $\forall \vec{z} \neq 0$ c'est-\`{a}-dire:
		\begin{equation}
		\vec{z}^{t}V(x)\vec{z} \geq 0
		\end{equation}
		\item[c)] Soit $\vec{x}$ un vecteur al\'{e}atoire d'esp\'{e}rance $E(\vec{x})$ et de covariance-variance $V(\vec{x})$ et \textbf{A} une certaine matrice alors
		\begin{eqnarray}
		E(\mathbf{A}\vec{x})&=&\mathbf{A}E(\vec{x})\\
		V(\mathbf{A}\vec{x})&=&\mathbf{A}V(\vec{x})\mathbf{A}^{t}
		\end{eqnarray}
	\end{itemize}
	\item[c)] \uline{Loi Normale Multi-dimensionnelle} Le vecteur al\'{e}atoire $\vec{x}$ est dit gaussien si ses n composantes $x_{i}$ suivent des lois normales sur $\mathbb{R}$. On note alors $\vec{x} \sim \mathbf{N}(\vec{\mu}, \Sigma)$ o\`{u} $\vec{\mu} = E(\vec{x})$ et $\Sigma = V(\vec{x})$.
\end{itemize}
\paragraph{}
On rappelle  que $E(\hat{\beta}) = \beta$ et $\hat{\sigma}^{2}=\frac{SCR}{T-k}=\frac{\hat{u}^{t}\hat{u}}{T-k}$ est un estimateur sans biais de $\sigma^{2}$ $(E(\hat{\sigma}^{2})=\sigma^{2})$ et $V(\hat{\beta})=\sigma^{2}(\mathbf{X}^{t}\mathbf{X})^{-1}$, $\widehat{V(\hat{\beta})}=\hat{\sigma}^{2}(\mathbf{X}^{t}\mathbf{X})^{-1}$ et $E(\widehat{V(\hat{\beta})})=V(\hat{\beta})$. Si l'on fait l'hypoth\`{e}se suppl\'{e}mentaire que $\vec{u}$ est normalement distribu\'{e} 
alors $\vec{Y} = \mathbf{X}\vec{\beta}+\vec{u}$ est aussi gaussien et
\begin{equation}
\hat{\beta} \sim \mathcal{N}(\vec{\beta}, \sigma^{2}(\mathbf{X}^{t}\mathbf{X})^{-1})
\end{equation}
\begin{theorem}
	Si $\vec{u}$ est normalement distribu\'{e} alors $\hat{u} = \mathbf{M}\vec{y}=\mathbf{M}\vec{u}$ est tel que:
	\begin{equation}
	\frac{\hat{u}^{t}\hat{u}}{\sigma^{2}} \sim \chi_{T-k}^{2}
	\end{equation}
\end{theorem}
\begin{proof}
	Soit alors $\vec{v} = \frac{\vec{u}}{\sigma}$ alors: $E(\vec{v}) = \frac{1}{\sigma}E(\vec{u})=\vec{0}$ et $V(\vec{v}) = \mathbf{I}$. Donc:
	\begin{eqnarray}
	\vec{v} &\sim& \mathcal{N}(\vec{0},\mathbf{I})\\
	\vec{u}^{t}\hat{u} &=& \vec{u}^{t}\mathbf{M}^{t}\vec{u} = \vec{u}^{t}\mathbf{M}\vec{u}\\
	\frac{\hat{u}^{t}\hat{u}}{\sigma^{2}} &=& \vec{v}^{t}\mathbf{M}\vec{v}
	\end{eqnarray}
	\paragraph{}
	Comme $\mathbf{M}$ est sym\'{e}trique, il existe une base form\'{e}e de vecteurs propres dans laquelle $\mathbf{M}$ est diagonalisabe et donc on peut trouver une matrice orthogonale $(\mathbf{H}^{t}\mathbf{H}= \mathbf{I})$ telle que: $\mathbf{\bigvee} =\mathbf{H}^{t}\mathbf{M}\mathbf{H}$ o\`{u} $\mathbf{\bigvee}$ est diagonale.
	\begin{itemize}
		\item[1)] \uline{\textbf{P} a k valeurs propres \'{e}gales \`{a} 1:} En effet puisque $P=\mathbf{X(X^{t}X)^{-1}X}$ et comme si \textbf{A} et \textbf{B} sont deux matrices les valeurs propres non nulles de \textbf{AB} et \textbf{BA} sont les m\^{e}mes. Donc $\mathbf{X[(X^{t}X)^{-1}X^{t}]}$ et $\mathbf{[(X^{t}X)^{-1}X^{t}]X}$ ont les m\^{e}mes valeurs propres \'{e}gales \`{a} 1 (La seconde matrice \'{e}tant la mtrice identit\'{e} de dimension k).
		\item[2)] 
		\begin{equation}
		\bigvee =
		\begin{bmatrix}
		\mathbf{I_{k}}&\mathbf{0}\\
		\mathbf{0}&\mathbf{0}
		\end{bmatrix}
		\end{equation}
		Car puisque \textbf{P} a k valeurs propres \'{e}gales \`{a} 1, \textbf{M} a $(T-k)$ valeurs propres \'{e}gales \`{a} 1. En effet si $\lambda$ est valeur propres de \textbf{M}:
		\begin{eqnarray}
		\mathbf{M}\vec{x}&=&\lambda\vec{x}\\
		(\mathbf{I-P})\vec{x} = \lambda\vec{x} &\Longrightarrow& \mathbf{P}\vec{x} = (1-\lambda) \vec{x}
		\end{eqnarray}
		\item[3)] 
		\begin{equation}
		\vec{v}^{t}\mathbf{M}\vec{v} = \vec{v}^{t}\mathbf{IMI}\vec{v} = \vec{v}^{t}\mathbf{H^{t}HMH^{t}H}\vec{v} = \vec{w}^{t}\bigvee\vec{w}
		\end{equation}
		avec
		\begin{equation}
		\vec{W}=\mathbf{H^{t}}\vec{v} \Longrightarrow \vec{v}^{t}\mathbf{M}\vec{v} = \vec{w}^{t}\begin{bmatrix}
		\mathbf{I_{k}}&\mathbf{0}\\
		\mathbf{0}&\mathbf{0}
		\end{bmatrix}\vec{w} = \sum_{i=1}^{T-k}w_{i}^{2}
		\end{equation}
		et comme 
		\begin{equation}
		\vec{w} \sim \mathcal{N}(\mathbf{0},\mathbf{I}) \Longrightarrow \frac{\hat{u}^{t}\hat{u}}{\sigma^{2}} = \vec{v}^{t}\mathbf{M}\vec{v} = \sum_{i=1}^{T-k}w_{i}^{2} \sim \chi^{2}_{T-k}
		\end{equation}
	\end{itemize}
\end{proof}
\paragraph{NB:}
\begin{eqnarray}
V(\mathbf{H^{t}}\vec{v}) &=& E[(\mathbf{H^{t}}\vec{v}-E(\mathbf{H^{t}}\vec{v}))(\mathbf{H^{t}}\vec{v}-E(\mathbf{H^{t}}\vec{v}))^{t}] \\
&=& E(\mathbf{H^{t}}\vec{v}\vec{v}^{t}\mathbf{H}) = \mathbf{H^{t}}E(\vec{v}\vec{v}^{t})\mathbf{H} = \mathbf{I}
\end{eqnarray}
\begin{theorem}[Multivariate C.L.T]
	Soient $\vec{X}_{1}, \vec{X}_{2}, \cdots, \vec{X}_{n}$ des vecteurs al\'{e}atoires i.i.d avec:
	\begin{equation}
	\vec{X}_{i} = \begin{pmatrix}
	X_{1i}\\X_{2i}\\ \vdots\\ X_{ki}
	\end{pmatrix}
	\end{equation}
	et
	\begin{equation}
	\vec{\mu} = \begin{pmatrix}
	E(X_{1i})\\E(X_{2i})\\ \vdots\\ E(X_{ki})
	\end{pmatrix}
	\end{equation}
	et pour matrice variance-covariance $\Sigma$. Alors:
	\begin{eqnarray}
	\sqrt{n}(\overline{X}-\vec{\mu}) &\overset{d}{\longrightarrow}& \mathcal{N}(\mathbf{0},\Sigma)\\
	\overline{X} &=& \begin{pmatrix}
	\overline{X}_{1}\\\overline{X}_{2}\\ \vdots\\ \overline{X}_{k}
	\end{pmatrix}\\
	\overline{X}_{j} &=& \frac{1}{n} \sum_{i=1}^{n}X_{ji}
	\end{eqnarray}
\end{theorem}
\begin{theorem}[Multivariate Delta Method]
	Supposons que: $Y_{n} =(Y_{n1}, Y_{n1}, \cdots, Y_{nk})$ soit une suite de variables al\'{e}atoires telle que:
	\begin{equation}
	\sqrt{n}(Y_{n}-\vec{\mu}) \overset{d}{\longrightarrow} \mathcal{N}(\mathbf{0},\Sigma)
	\end{equation}
	Soit alors $g: \mathbb{R}^{k} \longrightarrow \mathbb{R}$ telle que:
	\begin{equation}
	\overline{V}(g(\vec{y})) =
	\begin{pmatrix}
	\frac{\partial g}{\partial y_{1}}\\\frac{\partial g}{\partial y_{2}}\\ \vdots\\ \frac{\partial g}{\partial y_{k}}
	\end{pmatrix}
	\end{equation}
\end{theorem}
Notons $\overline{V}_{\vec{\mu}}$ la valeur de $\overline{V}(g(\vec{y}))$ \'{e}valu\'{e}e \`{a} $\vec{y}= \vec{\mu}$ valeur diff\'{e}rente de $\mathbf{0}$. Alors:
\begin{equation}
\sqrt{n}(g(Y_{n})-g(\vec{\mu})) \overset{d}{\longrightarrow} \mathcal{N}(\mathbf{0},\overline{V}_{\vec{\mu}}^{t}\Sigma\overline{V}_{\vec{\mu}})
\end{equation}
\begin{example}
	$\begin{pmatrix}
	X_{11}\\X_{21}
	\end{pmatrix},
	\begin{pmatrix}
	X_{12}\\X_{22}
	\end{pmatrix},
	\cdots,
	\begin{pmatrix}
	X_{1n}\\X_{2n}
	\end{pmatrix}$
	i.i.d vecteurs al\'{e}atoires de moyenne $\vec{\mu} = (\mu_{1},\mu_{2})$ et de variance $\Sigma$ et de variance $\Sigma$ et $\overline{X_{1}}=\frac{1}{n}\sum_{i=1}^{n}X_{1i}$, $\overline{X_{2}}=\frac{1}{n}\sum_{i=1}^{n}X_{2i}$. On d\'{e}finit $Y_{n} = \overline{X}_{1}\overline{X}_{2}$ et $g(\overline{X}_{1},\overline{X}_{2}) = g(Y_{n})$ o\`{u} $g(s_{1},s_{2})=s_{1}s_{2}$. Utilisant le M.C.L.T:
	\begin{equation}
	\sqrt{n} \begin{pmatrix}
	\overline{X}_{1}-\mu_{1}\\\overline{X}_{2}-\mu_{2}
	\end{pmatrix}
	\overset{d}{\longrightarrow} \mathcal{N}(\mathbf{0},\Sigma)
	\end{equation}
	et
	\begin{equation}
	\overline{V}(g(s)) = \begin{pmatrix}
	\frac{\partial g}{\partial s_{1}}\\ \frac{\partial g}{\partial s_{2}}
	\end{pmatrix} = \begin{pmatrix}
	s_{1}\\ s_{2}
	\end{pmatrix}
	\end{equation}
	\begin{equation}
	\overline{V}_{n}(\overline{X}_{1}\overline{X}_{2})\overset{d}{\longrightarrow}\mathcal{N}(0,\mu_{2}^{2}\sigma_{11}+2\mu_{1}\mu_{2}\sigma_{12}+\mu_{1}^{2}\sigma_{22})
	\end{equation}
	avec
	\begin{equation}
	\overline{V}_{\mu}^{t}\Sigma\overline{V}_{\mu} = \begin{pmatrix}
	\mu_{2}&\mu_{1}
	\end{pmatrix}
	\begin{pmatrix}
	\sigma_{11}&\sigma_{12}\\ \sigma_{12}&\sigma_{22}
	\end{pmatrix}
	\begin{pmatrix}
	\mu_{2} \\ \mu_{1}
	\end{pmatrix}
	\end{equation}
\end{example}
\newpage